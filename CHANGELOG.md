Latest version: https://gitlab.com/lysxia/ap-normalize/-/blob/main/CHANGELOG.md

## 0.1.0.1

- No library changes.
- Fix test suite to build with clang's C preprocessor (default on MacOS).

## 0.1.0.0

- Create ap-normalize.
